/*
 * console.h
 *
 *  Created on: 2021. szept. 21.
 *      Author: Kicsigamer
 */

#ifndef MAIN_CONSOLE_H_
#define MAIN_CONSOLE_H_


#define CONSOLE_TASK_SIZE (4096)

#define CONSOLE_HIST_LINE_SIZE (100)

void console_init();

#endif /* MAIN_CONSOLE_H_ */
