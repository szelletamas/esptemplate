#include <output_module/output.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "driver/gpio.h"

#include "main.h"

#include "template_module.h"
#include "console.h"

#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"

#define INIT_TEXT "app_main"

static TaskHandle_t template_taskHandler;
static TaskHandle_t output_taskHandler;

void app_main(void)
{
	esp_err_t ret;

	// Initialize NVS.
	ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
	    ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK( ret );


	esp_log_level_set(INIT_TEXT, ESP_LOG_DEBUG);
	ESP_LOGD(INIT_TEXT, "Enter app main");
    //Allow other core to finish initialization


	vTaskDelay(pdMS_TO_TICKS(100));

	init_OS();

	console_init();
	init_template();
}


/*
 * init freertos
 * */
void init_OS(){
	xTaskCreate(fn_template, "control", TEMPLATE_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), &template_taskHandler);
	xTaskCreate(fn_output, "output", TEMPLATE_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), &output_taskHandler);
}

/*
 * thread functions
 * */
void fn_template(){



	while(1){
		template_module_process();

		vTaskDelay(pdMS_TO_TICKS(1000));
	}
}

void fn_output()
{
	while(1){

		output_process();
		vTaskDelay(pdMS_TO_TICKS(5000));
	}
}
