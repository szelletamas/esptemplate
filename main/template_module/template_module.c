/*
 * template_module.c
 *
 *  Created on: 2021. szept. 18.
 *      Author: Kicsigamer
 */

#include <stdio.h>
#include "argtable3/argtable3.h"
#include "esp_console.h"

#define CMD_TEMPLATE "template"
#define HELP_TEMPLATE "this is a template"
#define HINT_TEMPLATE "use commands to make your life easier"

int counter=0;

void template_module_process(){
	counter++;
}


/*
 * commands
 * */
static struct{
	struct arg_lit *help;
	struct arg_lit *greet;
	struct arg_lit *counter;
	struct arg_end *end;
}template_args;

#define TEMPLATE_HELP "help"
#define TEMPLATE_HINT "use help for help greet for greeting"

#define TEMPLATE_GREET "greet"
#define TEMPLATE_HINT_GREET "greeet"

#define TEMPLATE_COUNTER "counter"
#define TEMPLATE_HINT_COUNTER "show counter"

static int cmd_template(int argc, char** argv){
	int nerr=arg_parse(argc, argv, (void**) &template_args);

	if(template_args.counter->count==1){
		printf("Current iteration: %d\n",counter);
		return 1;
	}

	if(template_args.greet->count==1){
			printf("Hello my friend in this video i will show you how to greet\n");
			return 1;
	}

	if(template_args.help->count==1){
			printf("use help for help greet for greeting counter for iteration write\n");
			return 1;
	}

	printf("consooole\n\r");
	return 1;
}

/*
 * initialization
 * */
void init_template(){
	template_args.help=arg_lit0("hH", TEMPLATE_HELP, TEMPLATE_HINT);
	template_args.greet=arg_lit0("gG", TEMPLATE_GREET, TEMPLATE_HINT_GREET);
	template_args.counter=arg_lit0("cC", TEMPLATE_COUNTER, TEMPLATE_HINT_COUNTER);
	template_args.end=arg_end(0);

	const esp_console_cmd_t cmd={
			.command=CMD_TEMPLATE,
			.help=HELP_TEMPLATE,
			.hint=HINT_TEMPLATE,
			.func=&cmd_template
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}
