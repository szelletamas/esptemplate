/*
 * template_module.h
 *
 *  Created on: 2021. szept. 18.
 *      Author: Kicsigamer
 */

#ifndef MAIN_TEMPLATE_MODULE_H_
#define MAIN_TEMPLATE_MODULE_H_

void template_module_process();

void init_template();

#endif /* MAIN_TEMPLATE_MODULE_H_ */
