/*
 * main.h
 *
 *  Created on: 2021. szept. 18.
 *      Author: Kicsigamer
 */

#ifndef MAIN_MAIN_H_
#define MAIN_MAIN_H_

#define TEMPLATE_STACK_SIZE (4096)

void init_OS();

void fn_template();

void fn_output();

#endif /* MAIN_MAIN_H_ */
